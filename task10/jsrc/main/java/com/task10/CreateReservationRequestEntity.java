package com.task10;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGenerateStrategy;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBGeneratedUuid;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "cmtr-a9f3b48c-Reservations-test")
public class CreateReservationRequestEntity {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;

    @DynamoDBAttribute
    private Integer tableNumber;

    @DynamoDBAttribute
    private String clientName;

    @DynamoDBAttribute
    private String phoneNumber;

    @DynamoDBAttribute
    private String date;

    @DynamoDBAttribute
    private String slotTimeStart;

    @DynamoDBAttribute
    private String slotTimeEnd;
}
