package com.task10;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.*;

import java.io.IOException;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static software.amazon.awssdk.services.cognitoidentityprovider.model.AuthFlowType.ADMIN_USER_PASSWORD_AUTH;
import static software.amazon.awssdk.services.cognitoidentityprovider.model.ChallengeNameType.NEW_PASSWORD_REQUIRED;
import static software.amazon.awssdk.services.cognitoidentityprovider.model.ExplicitAuthFlowsType.ALLOW_ADMIN_USER_PASSWORD_AUTH;
import static software.amazon.awssdk.services.cognitoidentityprovider.model.ExplicitAuthFlowsType.ALLOW_REFRESH_TOKEN_AUTH;
import static software.amazon.awssdk.services.cognitoidentityprovider.model.ExplicitAuthFlowsType.ALLOW_USER_PASSWORD_AUTH;

@LambdaHandler(lambdaName = "api_handler",
	roleName = "api_handler-role"
)
@Slf4j
public class ApiHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final CognitoIdentityProviderClient cognitoClient;
    private static final String USER_POOL_NAME = "cmtr-a9f3b48c-simple-booking-userpool-test";
    private static final String USER_POOL_CLIENT_NAME = "cmtr-a9f3b48c-simple-booking-userpool-client-test";

    private static final Pattern TABLES_PATTERN = Pattern.compile("/tables/(\\d+)");

    private static final List<String> DISABLE_AUTH_ENDPOINTS = Arrays.asList("/signup","/signin");

    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");

    private static final Pattern PASSWORD_PATTERN = Pattern.compile("[a-zA-Z0-9$%^*\\-_]{12,}");

    public ApiHandler() {
        cognitoClient = createCognitoIdentityProviderClient();
    }

	public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context) {
        String resource = event.getPath();
        String httpMethod = event.getHttpMethod();
        log.info("Receiving request {} {}", httpMethod, resource);

        String authToken = event.getHeaders().getOrDefault("Authorization", null);
        log.info("Auth token: {}", authToken);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
        response.setHeaders(headers);

        if (!DISABLE_AUTH_ENDPOINTS.contains(resource) && !verifyAuthToken(authToken)) {
            log.info("Authorization failed");
            response.setStatusCode(403);
            return response;
        }

        try {
            if (resource.equals("/signup") && httpMethod.equals("POST")) {
                signUp(event, response);
            } else if (resource.equals("/signin") && httpMethod.equals("POST")) {
                signIn(event, response);
            } else if (resource.equals("/tables") && httpMethod.equals("POST")) {
                createTable(event, response);
            } else if (resource.equals("/tables") && httpMethod.equals("GET")) {
                listTables(event, response);
            } else if (TABLES_PATTERN.matcher(resource).matches() && httpMethod.equals("GET")) {
                getTableById(event, response);
            } else if (resource.equals("/reservations") && httpMethod.equals("POST")) {
                createReservation(event, response);
            } else if (resource.equals("/reservations") && httpMethod.equals("GET")) {
                listReservations(event, response);
            } else {
                response.setStatusCode(400);
                response.setBody("{\"message\": \"Bad Request\"}");
                return response;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            response.setStatusCode(400);
            response.setBody(String.format("{\"message\": \"%s\"}", ExceptionUtils.getStackTrace(e)));
        }

        return response;
	}

    private boolean verifyAuthToken(String authToken) {
        try {
            authToken = authToken.substring(7);
            String[] splitToken = authToken.split("\\."); // Split the token into an array
            String base64EncodedBody = splitToken[1]; // Select the encoded body
            Base64.Decoder decoder = Base64.getDecoder();
            String body = new String(decoder.decode(base64EncodedBody));

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Body claims = objectMapper.readValue(body, Body.class);
            Date expirationDate = new Date((long) claims.exp * 1000);
            return expirationDate.after(new Date());
        } catch (Exception e) {
           return false;
        }
    }

    static class Body {
        public int exp;
    }

    private void signUp(APIGatewayProxyRequestEvent requestEvent, APIGatewayProxyResponseEvent responseEvent)
            throws IOException {
        log.info("Signup request. Request body {}", requestEvent.getBody());
        SignUpModel signUpModel = new ObjectMapper().readValue(requestEvent.getBody(), SignUpModel.class);
        if (!isSignUpModelValid(signUpModel)) {
            responseEvent.setStatusCode(400);
            return;
        }

        AttributeType email = AttributeType.builder()
                .name("email")
                .value(signUpModel.getEmail())
                .build();
//
//        AttributeType firstName = AttributeType.builder()
//                .name("custom:firstName")
//                .value(signUpModel.getFirstName())
//                .build();
//
//        AttributeType lastName = AttributeType.builder()
//                .name("custom:lastName")
//                .value(signUpModel.getLastName())
//                .build();

        AdminCreateUserRequest createUserRequest = AdminCreateUserRequest.builder()
                .username(signUpModel.getEmail())
                .userPoolId(getUserPoolId())
                .temporaryPassword(signUpModel.getPassword())
                .userAttributes(email)//, firstName, lastName)
                .messageAction(MessageActionType.SUPPRESS)
                .build();

        cognitoClient.adminCreateUser(createUserRequest);
        responseEvent.setStatusCode(200);
    }

    private boolean isSignUpModelValid(SignUpModel signUpModel) {
        if (StringUtils.isBlank(signUpModel.getFirstName())
                || StringUtils.isBlank(signUpModel.getLastName())
                || StringUtils.isBlank(signUpModel.getEmail())
                || StringUtils.isBlank(signUpModel.getPassword())) {
            log.error("Some of the fields are empty");
            return false;
        }

        if (!EMAIL_PATTERN.matcher(signUpModel.getEmail()).matches()) {
            log.error("Invalid email");
            return false;
        }

        if (!PASSWORD_PATTERN.matcher(signUpModel.getPassword()).matches()) {
            log.error("Invalid password");
            return false;
        }

        return true;
    }

    private void signIn(APIGatewayProxyRequestEvent requestEvent, APIGatewayProxyResponseEvent responseEvent)
            throws IOException {
        log.info("Sign in request. Request body {}", requestEvent.getBody());

        SignInModel signInModel = new ObjectMapper().readValue(requestEvent.getBody(), SignInModel.class);

        Map<String, String> authParams = new HashMap<>();
        authParams.put("USERNAME", signInModel.getEmail());
        authParams.put("PASSWORD", signInModel.getPassword());

        AdminInitiateAuthRequest request = AdminInitiateAuthRequest.builder()
                .userPoolId(getUserPoolId())
                .clientId(getClientId())
                .authParameters(authParams)
                .authFlow(ADMIN_USER_PASSWORD_AUTH)
                .build();

        try {
            AdminInitiateAuthResponse authResponse = cognitoClient.adminInitiateAuth(request);
            log.info("Auth response succeeded. HasChallenge: {} Challenge name (null if not found): {}",
                    authResponse.hasChallengeParameters(), authResponse.challengeName());
            if (authResponse.hasChallengeParameters() && NEW_PASSWORD_REQUIRED.equals(authResponse.challengeName())) {
                log.info("New password is required. Sending request to provide a new password. Session {}", authResponse.session());

                Map<String, String> responses  = new HashMap<>();
                responses.put("NEW_PASSWORD", signInModel.getPassword());
                responses.put("USERNAME", signInModel.getEmail());

                RespondToAuthChallengeRequest respondToAuthChallengeRequest = RespondToAuthChallengeRequest.builder()
                        .clientId(getClientId())
                        .challengeName(NEW_PASSWORD_REQUIRED)
                        .challengeResponses(responses)
                        .session(authResponse.session())
                        .build();

                RespondToAuthChallengeResponse authChallengeResult = cognitoClient.respondToAuthChallenge(respondToAuthChallengeRequest);


                SignInResponseEntity response = SignInResponseEntity.builder()
                        .accessToken(authChallengeResult.authenticationResult().idToken())
                        .build();

                log.info("Auth challenge result: {}", response.getAccessToken());

                responseEvent.setStatusCode(200);
                responseEvent.setBody(new ObjectMapper().writeValueAsString(response));
                return;
            }


            if (authResponse.authenticationResult() != null && authResponse.authenticationResult().idToken() != null) {
                SignInResponseEntity response = SignInResponseEntity.builder()
                        .accessToken(authResponse.authenticationResult().idToken())
                        .build();

                log.info("Token {}", authResponse.authenticationResult().idToken());

                responseEvent.setStatusCode(200);
                responseEvent.setBody(new ObjectMapper().writeValueAsString(response));
                return;
            }
        } catch (NotAuthorizedException exception) {
            responseEvent.setStatusCode(400);
        }


        responseEvent.setStatusCode(400);
    }

    private void createTable(APIGatewayProxyRequestEvent requestEvent, APIGatewayProxyResponseEvent responseEvent)
            throws IOException {
        log.info("Create table request. Request body {}", requestEvent.getBody());
        CreateTableRequestEntity createTableRequest = new ObjectMapper().readValue(requestEvent.getBody(), CreateTableRequestEntity.class);

        if (!isTableRequestValid(createTableRequest)) {
            responseEvent.setStatusCode(400);
            return;
        }

        createDynamoDbMapper().save(createTableRequest);

        CreateTableResponseEntity result = CreateTableResponseEntity.builder()
                .id(createTableRequest.getId())
                .build();

        responseEvent.setStatusCode(200);
        responseEvent.setBody(new ObjectMapper().writeValueAsString(result));
    }

    private boolean isTableRequestValid(CreateTableRequestEntity createTableRequest) {
        if (createTableRequest.getId() == null
                || createTableRequest.getNumber() == null
                || createTableRequest.getPlaces() == null
                || createTableRequest.getIsVip() == null) {
            log.info("Empty table parameters");
            return false;
        }

        return true;
    }

    private void listTables(APIGatewayProxyRequestEvent requestEvent, APIGatewayProxyResponseEvent responseEvent)
            throws JsonProcessingException {
        log.info("List tables request");

        List<CreateTableRequestEntity> tables = createDynamoDbMapper().scan(CreateTableRequestEntity.class, new DynamoDBScanExpression());

        responseEvent.setBody(new ObjectMapper().writeValueAsString(new GetTablesResponseEntity(tables)));
        responseEvent.setStatusCode(200);
    }

    private void getTableById(APIGatewayProxyRequestEvent requestEvent, APIGatewayProxyResponseEvent responseEvent)
            throws JsonProcessingException {

        String tableId = requestEvent.getPathParameters().getOrDefault("tableId", null);
        log.info("Get table request. Table id {}", tableId);


        DynamoDBQueryExpression<CreateTableRequestEntity> queryExpression = new DynamoDBQueryExpression<CreateTableRequestEntity>()
                .withHashKeyValues(CreateTableRequestEntity.builder()
                        .id(Integer.valueOf(tableId))
                        .build());

        Optional<CreateTableRequestEntity> table = createDynamoDbMapper().query(CreateTableRequestEntity.class,
                        queryExpression).stream()
                .findFirst();

        if (!table.isPresent()) {
            responseEvent.setStatusCode(404);
            return;
        }

        responseEvent.setBody(new ObjectMapper().writeValueAsString(table.get()));
        responseEvent.setStatusCode(200);
    }

    private void createReservation(APIGatewayProxyRequestEvent requestEvent, APIGatewayProxyResponseEvent responseEvent)
            throws IOException {
        log.info("Create reservation request. Request body {}", requestEvent.getBody());
        CreateReservationRequestEntity createReservationRequest = new ObjectMapper().readValue(requestEvent.getBody(),
                CreateReservationRequestEntity.class);

        if (!isValid(createReservationRequest)) {
            responseEvent.setStatusCode(400);
            return;
        }

        createDynamoDbMapper().save(createReservationRequest);
        CreateReservationResponseEntity response = new CreateReservationResponseEntity(createReservationRequest.getId());

        responseEvent.setStatusCode(200);
        responseEvent.setBody(new ObjectMapper().writeValueAsString(response));
    }

    private boolean isValid(CreateReservationRequestEntity createReservationRequest) {
        if (createReservationRequest.getTableNumber() == null
                || StringUtils.isBlank(createReservationRequest.getClientName())
                || StringUtils.isBlank(createReservationRequest.getPhoneNumber())
                || StringUtils.isBlank(createReservationRequest.getDate())
                || StringUtils.isBlank(createReservationRequest.getSlotTimeStart())
                || StringUtils.isBlank(createReservationRequest.getSlotTimeEnd())
                || !isTableExist(createReservationRequest.getTableNumber())) {
            return false;
        }

        if (isReservationAlreadyTaken(createReservationRequest)) {
            return false;
        }

        return true;
    }

    private boolean isReservationAlreadyTaken(CreateReservationRequestEntity createReservationRequest) {
        return getReservationsForTable(createReservationRequest.getTableNumber()).stream()
                .anyMatch(existingReservation -> haveOverlap(existingReservation, createReservationRequest));
    }

    private List<CreateReservationRequestEntity> getReservationsForTable(Integer tableNumber) {
        return createDynamoDbMapper().scan(CreateReservationRequestEntity.class, new DynamoDBScanExpression())
                .stream().filter(reservation -> Objects.equals(reservation.getTableNumber(), tableNumber))
                .collect(Collectors.toList());
    }

    private boolean haveOverlap(CreateReservationRequestEntity existingReservation, CreateReservationRequestEntity reservationToCreate) {
        if (!existingReservation.getDate().equals(reservationToCreate.getDate())) {
            return false;
        }
        
        log.info("Existing reservation {} {}",existingReservation.getSlotTimeStart(), existingReservation.getSlotTimeEnd());
        log.info("New reservation {} {}",reservationToCreate.getSlotTimeStart(), reservationToCreate.getSlotTimeEnd());

        LocalTime existingStartTime = LocalTime.parse(existingReservation.getSlotTimeStart());
        LocalTime existingEndTime = LocalTime.parse(existingReservation.getSlotTimeEnd());

        LocalTime newStartTime = LocalTime.parse(reservationToCreate.getSlotTimeStart());
        LocalTime newEndTime = LocalTime.parse(reservationToCreate.getSlotTimeEnd());


        boolean result = (newStartTime.isAfter(existingStartTime) && newStartTime.isBefore(existingEndTime))
                || (newEndTime.isAfter(existingStartTime) && newEndTime.isBefore(existingEndTime))
                || newStartTime.equals(existingStartTime)
                || newEndTime.equals(existingEndTime);

        if (result) {
            log.info("Overlap found");
        } else {
            log.info("Overlap not found");
        }

        return result;
    }

    private void listReservations(APIGatewayProxyRequestEvent requestEvent, APIGatewayProxyResponseEvent responseEvent)
            throws JsonProcessingException {
        List<CreateReservationRequestEntity> reservations = createDynamoDbMapper().scan(
                CreateReservationRequestEntity.class, new DynamoDBScanExpression());

        GetReservationsResponseEntity response = new GetReservationsResponseEntity(reservations);

        responseEvent.setBody(new ObjectMapper().writeValueAsString(response));
        responseEvent.setStatusCode(200);
    }

    private CognitoIdentityProviderClient createCognitoIdentityProviderClient() {
         return CognitoIdentityProviderClient.builder()
                .build();
    }

    private String getClientId() {
        String poolId = getUserPoolId();

        log.info("Trying to retrieve a client for a pool");

        ListUserPoolClientsResponse poolClients = cognitoClient.listUserPoolClients(
                ListUserPoolClientsRequest.builder()
                        .userPoolId(poolId)
                        .build());

        if (poolClients.hasUserPoolClients() && !poolClients.userPoolClients().isEmpty()) {
            log.info("Client found. Id {}", poolClients.userPoolClients().get(0).clientId());
            return poolClients.userPoolClients().get(0).clientId();
        }

        return createUserPoolClient(poolId).clientId();
    }

    private String getUserPoolId() {
        log.info("Trying to find user pool with name {}", USER_POOL_NAME);

        ListUserPoolsRequest listUserPoolsRequest = ListUserPoolsRequest.builder()
                .maxResults(60)
                .build();

        ListUserPoolsResponse response = cognitoClient.listUserPools(listUserPoolsRequest);
        for (UserPoolDescriptionType userPool : response.userPools()) {
            if (USER_POOL_NAME.equals(userPool.name())) {
                log.info("User pool found. Id {}", userPool.id());
                return userPool.id();
            }
        }

        log.info("User pool not found");

        throw new RuntimeException("User pool not found");
    }

    private UserPoolClientType createUserPoolClient(String poolId) {
        log.info("Client not found. Trying to create client...");
        CreateUserPoolClientResponse createUserPoolClientResponse = cognitoClient.createUserPoolClient(
                CreateUserPoolClientRequest.builder()
                        .userPoolId(poolId)
                        .clientName(USER_POOL_CLIENT_NAME)
                        .explicitAuthFlows(ALLOW_ADMIN_USER_PASSWORD_AUTH, ALLOW_REFRESH_TOKEN_AUTH, ALLOW_USER_PASSWORD_AUTH)
                        .build());

        log.info("Created client id: {}", createUserPoolClientResponse.userPoolClient().clientId());

        return createUserPoolClientResponse.userPoolClient();
    }

    private DynamoDBMapper createDynamoDbMapper() {
        AmazonDynamoDB dynamoDB = AmazonDynamoDBClientBuilder.standard().build();
        return new DynamoDBMapper(dynamoDB, dynamoDBMapperConfig());
    }

    private DynamoDBMapperConfig dynamoDBMapperConfig() {
        return DynamoDBMapperConfig.builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .withTableNameOverride(
                        DynamoDBMapperConfig.TableNameOverride.withTableNamePrefix(""))
                .build();
    }

    private boolean isTableExist(Integer tableId) {
        return createDynamoDbMapper().scan(CreateTableRequestEntity.class, new DynamoDBScanExpression()).stream()
                .anyMatch(table -> table.getNumber().equals(tableId));
    }
}
