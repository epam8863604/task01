package com.task10;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "cmtr-a9f3b48c-Tables-test")
public class CreateTableRequestEntity {
    @DynamoDBHashKey
    private Integer id;

    @DynamoDBAttribute
    private Integer number;

    @DynamoDBAttribute
    private Integer places;

    @DynamoDBAttribute
    private Boolean isVip;

    @DynamoDBAttribute
    private Integer minOrder;
}
