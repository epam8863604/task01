package com.task10;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class GetReservationsResponseEntity {
    private List<CreateReservationRequestEntity> reservations;
}
