package com.task06;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue;
import com.syndicate.deployment.annotations.events.DynamoDbTriggerEventSource;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import lombok.extern.java.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;

@LambdaHandler(lambdaName = "audit_producer",
	roleName = "audit_producer-role"
)
@DynamoDbTriggerEventSource(targetTable = "Configuration", batchSize = 10)
@Log
public class AuditProducer implements RequestHandler<DynamodbEvent, Void> {

    private static final String INSERT_EVENT_NAME = "INSERT";
    private static final String MODIFY_EVENT_NAME = "MODIFY";

    private static final String KEY = "key";
    private static final String VALUE = "value";

    public Void handleRequest(DynamodbEvent event, Context context) {
        DynamoDBMapper mapper = createDynamoDbMapper();
        event.getRecords()
                .forEach(dynamodbStreamRecord -> {
                    log.log(Level.INFO,
                            "Received record from DynamoDB. Name of the event " + dynamodbStreamRecord.getEventName());
                    if (INSERT_EVENT_NAME.equals(dynamodbStreamRecord.getEventName())) {
                        addInsertAuditRecord(mapper, dynamodbStreamRecord);
                    } else if (MODIFY_EVENT_NAME.equals(dynamodbStreamRecord.getEventName())) {
                        addModifyAuditRecord(mapper, dynamodbStreamRecord);
                    }
                });
        return null;
    }

    private DynamoDBMapper createDynamoDbMapper() {
        AmazonDynamoDB dynamoDB = AmazonDynamoDBClientBuilder.standard().build();
        return new DynamoDBMapper(dynamoDB, dynamoDBMapperConfig());
    }

    private DynamoDBMapperConfig dynamoDBMapperConfig() {
        return DynamoDBMapperConfig.builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .withTableNameOverride(
                        DynamoDBMapperConfig.TableNameOverride.withTableNamePrefix(""))
                .build();
    }

    private void addInsertAuditRecord(DynamoDBMapper mapper, DynamodbEvent.DynamodbStreamRecord dynamodbStreamRecord) {
        String key = dynamodbStreamRecord.getDynamodb().getNewImage().get(KEY).getS();
        String value = dynamodbStreamRecord.getDynamodb().getNewImage().getOrDefault(VALUE, new AttributeValue()).getS();
        String modificationDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(new Date());

        Map<String, String> newValue = new LinkedHashMap<>();
        newValue.put(KEY, key);
        newValue.put(VALUE, value);

        AuditCreateModel model = AuditCreateModel.builder()
                .itemKey(key)
                .modificationTime(modificationDate)
                .newValue(newValue)
                .build();

        mapper.save(model);
    }
    private void addModifyAuditRecord(DynamoDBMapper mapper, DynamodbEvent.DynamodbStreamRecord dynamodbStreamRecord) {
        String newKey = dynamodbStreamRecord.getDynamodb().getNewImage().get(KEY).getS();
        String newValue = dynamodbStreamRecord.getDynamodb().getNewImage().getOrDefault(VALUE, new AttributeValue()).getS();

        String oldValue = dynamodbStreamRecord.getDynamodb().getOldImage().getOrDefault(VALUE, new AttributeValue()).getS();

        String modificationDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(
                dynamodbStreamRecord.getDynamodb().getApproximateCreationDateTime());

        AuditUpdateModel model = AuditUpdateModel.builder()
                .itemKey(newKey)
                .modificationTime(modificationDate)
                .updatedAttribute(VALUE)
                .oldValue(oldValue)
                .newValue(newValue)
                .build();

        mapper.save(model);
    }
}
