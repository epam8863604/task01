package com.task07;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.syndicate.deployment.annotations.events.RuleEventSource;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@LambdaHandler(lambdaName = "uuid_generator",
	roleName = "uuid_generator-role"
)
@RuleEventSource(targetRule = "uuid_trigger")
public class UuidGenerator implements RequestHandler<Object, Void> {

    private static final String BUCKET_NAME = "cmtr-a9f3b48c-uuid-storage-test";

    public Void handleRequest(Object request, Context context) {
        AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

        ZonedDateTime now = ZonedDateTime.now();
        String fileName = now.format(DateTimeFormatter.ISO_INSTANT);

        String content = IntStream.range(0, 10)
                .mapToObj(i -> UUID.randomUUID().toString())
                .collect(Collectors.joining("\",\n        \""));

        File file = new File("/tmp/" + fileName); // AWS Lambda can write to /tmp/ directory

        try (FileWriter writer = new FileWriter(file)) {
            writer.write("{\n");
            writer.write("    \"ids\":[\n");
            writer.write("        \"" + content + "\"");
            writer.write("\n    ]\n");
            writer.write("}");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        s3Client.putObject(new PutObjectRequest(BUCKET_NAME, fileName, file));

        return null;
    }
}
