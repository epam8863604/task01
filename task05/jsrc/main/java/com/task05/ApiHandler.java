package com.task05;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import lombok.SneakyThrows;
import lombok.extern.java.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

@LambdaHandler(lambdaName = "api_handler",
	roleName = "api_handler-role"
)
@Log
public class ApiHandler implements RequestHandler<ApiRequestEntity, ApiResponseEntity> {

	@SneakyThrows
    public ApiResponseEntity handleRequest(ApiRequestEntity request, Context context) {
        EventModel eventModel = EventModel.builder()
                .principalId(request.getPrincipalId())
                .body(request.getContent())
                .build();

        DynamoDBMapper mapper = createDynamoDbMapper();
        mapper.save(eventModel);

        return ApiResponseEntity.builder()
                .statusCode(201)
                .event(EventResponseEntity.builder()
                        .id(eventModel.getId())
                        .principalId(eventModel.getPrincipalId())
                        .body(eventModel.getBody())
                        .createdAt(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(eventModel.getCreatedAt()))
                        .build())
                .build();
	}

    private DynamoDBMapper createDynamoDbMapper() {
        AmazonDynamoDB dynamoDB = AmazonDynamoDBClientBuilder.standard().build();
        return new DynamoDBMapper(dynamoDB, dynamoDBMapperConfig());
    }

    private DynamoDBMapperConfig dynamoDBMapperConfig() {
        return DynamoDBMapperConfig.builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .withTableNameOverride(
                        DynamoDBMapperConfig.TableNameOverride.withTableNamePrefix(""))
                .build();
    }
}
