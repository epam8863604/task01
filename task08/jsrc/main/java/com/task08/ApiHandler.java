package com.task08;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.syndicate.deployment.annotations.LambdaUrlConfig;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.annotations.lambda.LambdaLayer;
import com.syndicate.deployment.model.ArtifactExtension;
import com.syndicate.deployment.model.DeploymentRuntime;
import com.syndicate.deployment.model.lambda.url.AuthType;
import com.syndicate.deployment.model.lambda.url.InvokeMode;

@LambdaLayer(
        layerName = "open-meteo",
        libraries = { "lib/task08-1.0.0.jar" },
        runtime = DeploymentRuntime.JAVA8,
        artifactExtension = ArtifactExtension.ZIP)
//@LambdaHandler(lambdaName = "api_handler",
//        roleName = "api_handler-role"
//)
//@LambdaUrlConfig(
//        authType = AuthType.NONE,
//        invokeMode = InvokeMode.BUFFERED
//)
public class ApiHandler implements RequestHandler<Object, String> {

	public String handleRequest(Object request, Context context) {
        OpenMeteoAPI api = new OpenMeteoAPI();
        try {
            return api.getLatestForecast().toString();
        } catch (UnirestException e) {
            throw new RuntimeException(e);
        }
    }
}
