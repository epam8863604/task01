package com.task09;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGenerateStrategy;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBGeneratedUuid;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "cmtr-a9f3b48c-Weather-test")
public class ForecastModel {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;

    @DynamoDBAttribute
    private Forecast forecast;

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @DynamoDBDocument
    public static class Forecast {
        @DynamoDBAttribute
        private Integer elevation;

        @DynamoDBAttribute
        private Long generationtime_ms;

        @DynamoDBAttribute
        private Hourly hourly;

        @DynamoDBAttribute
        private HourlyUnits hourly_units;

        @DynamoDBAttribute
        private Double latitude;

        @DynamoDBAttribute
        private Double longitude;

        @DynamoDBAttribute
        private String timezone;

        @DynamoDBAttribute
        private String timezone_abbreviation;

        @DynamoDBAttribute
        private Integer utc_offset_seconds;

        @Getter
        @Setter
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        @DynamoDBDocument
        public static class Hourly {
            @DynamoDBAttribute
            private List<Double> temperature_2m;

            @DynamoDBAttribute
            private List<String> time;
        }

        @Getter
        @Setter
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        @DynamoDBDocument
        public static class HourlyUnits {
            @DynamoDBAttribute
            private String temperature_2m;

            @DynamoDBAttribute
            private String time;
        }
    }
}
