package com.task09;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.handlers.TracingHandler;
import com.amazonaws.xray.proxies.apache.http.HttpClientBuilder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.syndicate.deployment.annotations.lambda.LambdaLayer;
import com.syndicate.deployment.model.ArtifactExtension;
import com.syndicate.deployment.model.DeploymentRuntime;
import com.task08.OpenMeteoAPI;

import java.io.IOException;
import java.util.Map;

@LambdaLayer(
        layerName = "open-meteo",
        libraries = { "lib/task08-1.0.0.jar" },
        runtime = DeploymentRuntime.JAVA8,
        artifactExtension = ArtifactExtension.ZIP)
public class Processor implements RequestHandler<Object, String> {

	public String handleRequest(Object request, Context context) {
        Unirest.setHttpClient(HttpClientBuilder.create().build());
        OpenMeteoAPI api = new OpenMeteoAPI();
        try {
            JsonNode forecast = api.getLatestForecast();

            createDynamoDbMapper().save(ForecastModel.builder()
                    .forecast(new ObjectMapper().readValue(forecast.toString(), ForecastModel.Forecast.class)).build());
            return forecast.toString();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private DynamoDBMapper createDynamoDbMapper() {
        AmazonDynamoDB dynamoDB = AmazonDynamoDBClientBuilder.standard()
                .withRequestHandlers(new TracingHandler(AWSXRay.getGlobalRecorder()))
                .build();
        return new DynamoDBMapper(dynamoDB, dynamoDBMapperConfig());
    }

    private DynamoDBMapperConfig dynamoDBMapperConfig() {
        return DynamoDBMapperConfig.builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .withTableNameOverride(
                        DynamoDBMapperConfig.TableNameOverride.withTableNamePrefix(""))
                .build();
    }
}
