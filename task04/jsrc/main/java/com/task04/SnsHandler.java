package com.task04;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.syndicate.deployment.annotations.events.SnsEventSource;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import lombok.extern.java.Log;

import java.util.logging.Level;

@LambdaHandler(lambdaName = "sns_handler",
	roleName = "sns_handler-role"
)
@SnsEventSource(targetTopic = "lambda_topic")
@Log
public class SnsHandler implements RequestHandler<SNSEvent, Void> {


	public Void handleRequest(SNSEvent snsEvent, Context context) {
        log.log(Level.INFO, snsEvent.toString());
		return null;
	}
}
