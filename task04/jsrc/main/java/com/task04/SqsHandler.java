package com.task04;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.syndicate.deployment.annotations.events.SqsEvents;
import com.syndicate.deployment.annotations.events.SqsTriggerEventSource;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import lombok.extern.java.Log;

import java.util.logging.Level;

@LambdaHandler(lambdaName = "sqs_handler",
        roleName = "sqs_handler-role"
)
@SqsEvents(
        @SqsTriggerEventSource(targetQueue = "async_queue", batchSize = 5)
)
@Log
public class SqsHandler implements RequestHandler<SQSEvent, Void> {

    public Void handleRequest(SQSEvent sqsEvent, Context context) {
        log.log(Level.INFO, sqsEvent.toString());
        return null;
    }
}
